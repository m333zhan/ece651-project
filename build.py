# Build script for the project

import signal
import shlex
import sys
import subprocess
import argparse
import shutil
import swagger.buildswagger as swaggerSubScript
from enum import Enum

# Type enum


class RunType(Enum):
    Server = 0
    Web = 1
    Sql = 2
    Backend = 3
    All = 4


# General string const
docker_compose_file_path = './docker/docker-compose.yml'
docker_compose_local_env_file = './docker/env/local.env'
server_service_name = "studentcareserver"
web_service_name = "studentcareweb"
db_service_name = "studentcaredb"

# Server string const
server_src_path = "./ece651-server/src"
server_setup_sh = "serversetup.sh"

# Web string const
web_src_path = "./ece651-web/src"
web_setup_sh = "websetup.sh"

# Ctrl+C capture


def signal_handler(sig, frame):
    cleanArtifacts()


signal.signal(signal.SIGINT, signal_handler)


def setUpGitPrePushHook():
    # Change the permission of server/pre-push
    # Command: chmod +x git_hooks/server/pre-push
    process = subprocess.Popen(
        ["chmod", "+x", "git_hooks/server/pre-push"], stdout=sys.stdout)
    outs, _ = process.communicate()

    shutil.copyfile("git_hooks/server/pre-push",
                    ".git/modules/ece651-server/hooks/pre-push")

    # Change the permission of web/pre-push
    # Command: chmod +x git_hooks/web/pre-push
    process = subprocess.Popen(
        ["chmod", "+x", "git_hooks/web/pre-push"], stdout=sys.stdout)
    outs, _ = process.communicate()

    shutil.copyfile("git_hooks/web/pre-push",
                    ".git/modules/ece651-web/hooks/pre-push")


def initGitSubmodules():
    # Init git submodules
    # Command: git submodule update --init --recursive
    process = subprocess.Popen(
        ["git", "submodule", "update", "--init", "--recursive"], stdout=sys.stdout)
    outs, errs = process.communicate()

    if errs:
        print('Error in init git submodules: ', errs.decode())


def checkoutGitSubmodules():
    # Checkout remote submodules changes (main branch)
    # Command: git submodule update --remote --recursive
    process = subprocess.Popen(
        ["git", "submodule", "update", "--remote", "--recursive"], stdout=sys.stdout)
    outs, errs = process.communicate()

    if errs:
        print('Error in checking out remote submodules changes: ', errs.decode())


def cleanArtifacts():
    # Stop and remove all containers, images, and volumes
    # Command: docker-compose -f ./docker/docker-compose.yml
    #          --env-file ./docker/env/local.env down --rmi all --volumes --remove-orphans
    process = subprocess.Popen(shlex.split(
        f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
          down --rmi all --volumes --remove-orphans'), stdout=sys.stdout)
    outs, _ = process.communicate()


def run(type):
    cleanArtifacts()

    # Change the permission of serversetup.sh
    # Command: chmod +x serversetup.sh
    process = subprocess.Popen(
        ["chmod", "+x", server_src_path + "/" + server_setup_sh], stdout=sys.stdout)
    outs, _ = process.communicate()

    # Change the permission of websetup.sh
    # Command: chmod +x websetup.sh
    process = subprocess.Popen(
        ["chmod", "+x", web_src_path + "/" + web_setup_sh], stdout=sys.stdout)
    outs, _ = process.communicate()

    if type == RunType.All:
        # Run and publish server process locally
        # Command: docker compose -f ./docker/docker-compose.yml --env-file ./docker/env/local.env up --build
        process = subprocess.Popen(shlex.split(
            f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
              up --build'), stdout=sys.stdout)
        outs, _ = process.communicate()
    elif type == RunType.Server:
        # Run and publish server process locally
        # Command: docker compose -f ./docker/docker-compose.yml --env-file ./docker/env/local.env up <service_name> --build
        process = subprocess.Popen(shlex.split(
            f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
              up {server_service_name} --build'), stdout=sys.stdout)
        outs, _ = process.communicate()
    elif type == RunType.Web:
        # Run and publish web process locally
        # Command: docker compose -f ./docker/docker-compose.yml --env-file ./docker/env/local.env up <service_name> --build
        process = subprocess.Popen(shlex.split(
            f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
              up {web_service_name} --build'), stdout=sys.stdout)
        outs, _ = process.communicate()
    elif type == RunType.Sql:
        # Run and publish sql process locally
        # Command: docker compose -f ./docker/docker-compose.yml --env-file ./docker/env/local.env up <service_name> --build
        process = subprocess.Popen(shlex.split(
            f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
              up {db_service_name} --build'), stdout=sys.stdout)
        outs, _ = process.communicate()
    elif type == RunType.Backend:
        # Run and publish server and sql process together locally
        # Command: docker compose -f ./docker/docker-compose.yml --env-file ./docker/env/local.env up <service_name> --build
        process = subprocess.Popen(shlex.split(
            f'docker compose -f {docker_compose_file_path} --env-file {docker_compose_local_env_file} \
              up {server_service_name} {db_service_name} --build'), stdout=sys.stdout)
        outs, _ = process.communicate()


def parse_args():
    ap = argparse.ArgumentParser(description="Project build tool")

    ap.add_argument(
        '-s', '--setUpProject', default=False, action='store_true',
        help='Init and checkout submodules and install git pre-push hook')
    ap.add_argument(
        '-od', '--openSwaggerDoc', default=False, action='store_true',
        help='Open swagger doc on browser')
    ap.add_argument(
        '-oe', '--openSwaggerEditor', default=False, action='store_true',
        help='Open swagger editor on browser')
    ap.add_argument(
        '-c', '--clean', default=False, action='store_true',
        help='Clean all docker artifacts')

    # Local run
    subparser = ap.add_subparsers(dest='run')
    run = subparser.add_parser('run')
    run.add_argument(
        '-rs', '--runServerProcess', default=False, action='store_true',
        help='Run server process on docker container locally')
    run.add_argument(
        '-rw', '--runWebProcess', default=False, action='store_true',
        help='Run web process on docker container locally')
    run.add_argument(
        '-rsql', '--runSQLProcess', default=False, action='store_true',
        help='Run mysql process on docker container locally')
    run.add_argument(
        '-rbe', '--runBackendProcess', default=False, action='store_true',
        help='Run server and mysql process together on docker container locally')
    run.add_argument(
        '-rall', '--runAllProcess', default=False, action='store_true',
        help='Run all processes locally')

    parsed_args = ap.parse_args()

    return parsed_args


def main():
    parsed_args = parse_args()

    if parsed_args.setUpProject:
        initGitSubmodules()
        checkoutGitSubmodules()
        setUpGitPrePushHook()
    if parsed_args.openSwaggerDoc:
        swaggerSubScript.init()
        swaggerSubScript.run(False)
    if parsed_args.openSwaggerEditor:
        swaggerSubScript.init()
        swaggerSubScript.run(True)
    if parsed_args.clean:
        cleanArtifacts()

    if parsed_args.run:
        if parsed_args.runAllProcess:
            run(RunType.All)
        if parsed_args.runServerProcess:
            run(RunType.Server)
        if parsed_args.runWebProcess:
            run(RunType.Web)
        if parsed_args.runSQLProcess:
            run(RunType.Sql)
        if parsed_args.runBackendProcess:
            run(RunType.Backend)


if __name__ == "__main__":
    main()
