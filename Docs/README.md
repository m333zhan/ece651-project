# Docs

## Required

- [ECE 651 Project Description](https://docs.google.com/document/d/1lM_zuU0tde2NCeg7iwNtJYhk9NTMIHDCq-qyu5jdujQ/edit)
- [ECE651 Project Abstract](https://docs.google.com/document/d/1cTHpGkCXJRUaTb81JMdkg78OMebbdsmufNUWOf9fyzc/edit)
- [ECE651 Team Contract](https://docs.google.com/document/d/1Pn29kZw2TLfaHwiD65P-90W94ACT496ZpbCcOLXHMOA/edit)

## Functional
- [Meeting minute](https://docs.google.com/document/d/1I6EOQqbaDMgYY_MInW3mg9Sdo0Ssj_jz0HP6A3GZXKs/edit?usp=sharing)
- [Issue Creation Guideline](https://docs.google.com/document/d/1GU4P3oUNdVp4WPqV3cyssrZkzzO2X2U2-6-0TbO8_Q0/edit#heading=h.pyrxv7m08ksi)
- [Studentcare Test Plan](https://docs.google.com/spreadsheets/d/1McChc5ASWavdvnsTIaJd1877Li3AANS0PRVBIxUglJw/edit?usp=sharing)
- [DB Diagram](https://lucid.app/lucidchart/35624447-89f0-4d49-970e-56923ad0cfa4/edit?viewport_loc=-11%2C-119%2C2219%2C1065%2C0_0&invitationId=inv_525e69db-2a2b-4bb1-88ca-7030b1b39586)

## Midterm Review
- [Midterm Review Check List](https://docs.google.com/document/d/10g8YzGKD6J06PaK3SI3wK6KBy4-ZE_hn-6WHdRtwFE0/edit)
- [Midterm Review Presentation Notes](https://docs.google.com/presentation/d/1Gfdb4R3_HgziBINmM4UwvpBXktJAcOx6BYcv9g_7HTw/edit?usp=sharing)
- [Midterm Review Presentation](https://docs.google.com/presentation/d/1Iypfal9bA3swdEiXWiETG-j5rd8xCXaf3o_CUJ3xg8A/edit?usp=sharing)

## Final Review
- [Final Review Prensentation](https://docs.google.com/presentation/d/1H0VMNZToofIfSWLYXJuCEuU9QYOoH-1R9UFYnBgX_cU/edit?usp=sharing)
