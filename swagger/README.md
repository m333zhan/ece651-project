# ECE 651 Swagger Documentation

## Setup swagger locally

- By using ```buildswagger.py```, we could both display and edit API doc locally on browser.

- It relies on the [docker images](https://github.com/swagger-api) and the ```swagger.json```

> Editor host url: http://localhost/swagger-editor/

> UI host url: http://localhost/swagger/