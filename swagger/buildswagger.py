# Build script for swagger editor and ui

import subprocess
import argparse
import os
import webbrowser

# String const
publish_port = "80:8080"

# Editor
editor_image_tag = "swaggerapi/swagger-editor:v4.6.0"
editor_url = "http://localhost/swagger-editor/"
editor_container_name = "swaggerEditor"

# UI
ui_image_tag = "swaggerapi/swagger-ui:v4.15.3"
ui_url = "http://localhost/swagger/"
ui_container_name = "swaggerUI"

current_dir = os.path.dirname(os.path.abspath(__file__))


def cleanAllArtifacts(onlyContainer=False):
    runningImages = [ui_image_tag, editor_image_tag]
    runningContainers = [editor_container_name, ui_container_name]

    # Stop all containers
    # Command: docker stop -t 0 runningContainers
    arg_stop_containers = ["docker", "stop", "-t", "0"]
    if isinstance(runningContainers, str):
        arg_stop_containers.append(runningContainers)
    else:
        arg_stop_containers.extend(runningContainers)
    process = subprocess.Popen(
        arg_stop_containers, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    outs, _ = process.communicate()

    # Delete all containers
    # Command: docker rm runningContainers
    arg_delete_containers = ["docker", "rm"]
    if isinstance(runningContainers, str):
        arg_delete_containers.append(runningContainers)
    else:
        arg_delete_containers.extend(runningContainers)
    process = subprocess.Popen(
        arg_delete_containers, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    outs, _ = process.communicate()

    if not onlyContainer:
        # Remove all images
        # Command: docker rmi runningImages
        arg_delete_images = ["docker", "rmi"]
        if isinstance(runningImages, str):
            arg_delete_images.append(runningImages)
        else:
            arg_delete_images.extend(runningImages)
        process = subprocess.Popen(
            arg_delete_images, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        outs, _ = process.communicate()


def init():
    cleanAllArtifacts()

    # Pull editor and ui docker images
    # Command: docker pull tag
    process = subprocess.Popen(
        ["docker", "pull", editor_image_tag], stdout=subprocess.PIPE)
    outs, _ = process.communicate()

    process = subprocess.Popen(
        ["docker", "pull", ui_image_tag], stdout=subprocess.PIPE)
    outs, _ = process.communicate()


def run(isEditor):
    cleanAllArtifacts(True)

    if isEditor:
        # Run and publish editor process
        # Command: docker run --name editor_container_name -d -p 80:8080 -v $(pwd):/etc/nginx/html -e SWAGGER_FILE=/swagger/swagger.json
        #                     -e BASE_URL=/swagger-editor swaggerapi/swagger-editor
        process = subprocess.Popen(["docker", "run", "--name", editor_container_name, "-d", "--publish", publish_port,
                                    "-v", current_dir + ":/etc/nginx/html",
                                    "-e", "SWAGGER_FILE=/swagger/swagger.json", "-e", "BASE_URL=/swagger-editor",
                                    editor_image_tag], stdout=subprocess.PIPE)
        outs, _ = process.communicate()
    else:
        # Run and publish ui process
        # Command: docker run --name ui_container_name -d -p 80:8080 -v $(pwd):/swagger -e SWAGGER_JSON=/swagger/swagger.json
        #                     -e BASE_URL=/swagger swaggerapi/swagger-ui
        process = subprocess.Popen(["docker", "run", "--name", ui_container_name, "-d", "--publish", publish_port, "-v", current_dir + ":/swagger",
                                    "-e", "SWAGGER_JSON=/swagger/swagger.json", "-e", "BASE_URL=/swagger",
                                    ui_image_tag], stdout=subprocess.PIPE)
        outs, _ = process.communicate()

    openURL(isEditor)


def openURL(isEditor):
    webbrowser.open(editor_url if isEditor else ui_url, new=0, autoraise=True)


def parse_args():
    ap = argparse.ArgumentParser(description="Swagger build tool")

    ap.add_argument(
        '-ci', '--createImages', default=False, action='store_true',
        help='Create swagger docker images')
    ap.add_argument(
        '-res', '--runEditorProcess', default=False, action='store_true',
        help='Run swagger editor process on docker container')
    ap.add_argument(
        '-rus', '--runUIProcess', default=False, action='store_true',
        help='Run swagger ui process on docker container')
    ap.add_argument(
        '-c', '--clean', default=False, action='store_true',
        help='Clean all swagger docker artifacts')

    parsed_args = ap.parse_args()

    return parsed_args


def main():
    parsed_args = parse_args()

    if parsed_args.createImages:
        init()
    if parsed_args.runEditorProcess:
        run(True)
    if parsed_args.runUIProcess:
        run(False)
    if parsed_args.clean:
        cleanAllArtifacts()


if __name__ == "__main__":
    main()
